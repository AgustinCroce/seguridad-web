const Wallets = require("./wallets");
const Users = require("./users");

module.exports = {
  Wallets,
  Users
};