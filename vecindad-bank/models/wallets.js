const mongoose = require("mongoose");

const WalletsSchema = new mongoose.Schema({
  cvu: {type: String, required: true},
  balance: {type: Number, default: 0}
});

module.exports = mongoose.model("Wallets", WalletsSchema);