const mongoose = require("mongoose");

const UsersSchema = new mongoose.Schema({
  username: {type: String, required: true},
  rol: {type: String, enum: ["cliente", "admin"], default: "cliente"},
  cvu: {type: String, required: true},
});

module.exports = mongoose.model("Users", UsersSchema);