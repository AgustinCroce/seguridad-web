"use strict";
const fs = require("fs");
const jws = require("jws");
const publicKey = fs.readFileSync("./public.pem");
const express = require("express");
const st = require("st");
const mongoose = require("mongoose");
const {Users, Wallets} = require("./models");
const axios = require("axios");

const app = express();
app.use(express.json());
app.use(express.urlencoded());
app.use(st({path: './public', url: "/public"}));

async function loadUsers() {
  const cvuChavo = mongoose.Types.ObjectId("1d755cda43da9095a2300964");
  const cvuBarriga = mongoose.Types.ObjectId("2d755cda43da9095a2300964");
  const cvuAdmin = mongoose.Types.ObjectId("3d755cda43da9095a2300964");

  await Users.remove({});
  await Wallets.remove({});

  await Users.insertMany([{
    _id: mongoose.Types.ObjectId("5d755cda43da9095a2300964"),
    username: "chavodel8",
    cvu: cvuChavo,
    rol: "cliente"
  }, {
    _id: mongoose.Types.ObjectId("5d755cf243da9095a2300965"),
    username: "señorBarriga",
    cvu: cvuBarriga,
    rol: "cliente"
  }, {
    _id: mongoose.Types.ObjectId("5d755d0043da9095a2300966"),
    username: "admin",
    cvu: cvuAdmin,
    rol: "admin"
  }]);

  await Wallets.insertMany([
    {
      cvu: cvuChavo,
      balance: 0
    },
    {
      cvu: cvuBarriga,
      balance: 10000
    }
  ]);
}

function verifyToken(token) {
  const buff = new Buffer(token.split(".")[0], 'base64');
  const header = JSON.parse(buff.toString('ascii'));
  
  if (!jws.verify(token, header.alg, publicKey)) {
    throw new Error("Unauthorized");
  }

  return jws.decode(token);
}

function validateJwtMiddleware(req, res, next) {
  const jwt = req.headers.authorization ? req.headers.authorization.split(" ")[1]: "";
  
  try {
    const decoded = verifyToken(jwt);
    req.user = decoded.payload;
    return next();
  } catch (error) {
    return res.status(401).send();
  }
}

function validateRolMiddleware(rol) {
  return async (req, res, next) => {
    const user = await Users.findOne({_id: req.user.id, rol});

    if (!user) {
      return res.status(401).send();
    }

    req.user = user;
    next();
  }
}

app.set('view engine', 'ejs'); 

app.get('/', async (req, res) => {
  res.render('index.ejs');
});

app.get('/login', async (req, res) => {
  res.render('login.ejs');
});

app.post('/login', async (req, res) => {
  try {
    const {data: {jwt}} = await axios.post("http://auth-server:8080/login", {username: req.body.username, password: req.body.password});
    const {payload: user} = verifyToken(jwt);
    const userDoc = await Users.findById(user.id);
    const wallet = await Wallets.findOne({cvu: userDoc.cvu});
    res.redirect(`/profile?cvu=${wallet.cvu}&balance=${wallet.balance}`);
  } catch (error) {
    res.redirect('/login');
  }
});

app.get('/profile', async (req, res) => {
  res.render('profile.ejs', {
      wallet: {cvu: req.query.cvu, balance: req.query.balance}
  });
});

app.use(validateJwtMiddleware);

app.get("/users", validateRolMiddleware("admin"), async (req, res) => {
  const users = await Users.find({});
  return res.status(200).json(users);
});

app.get("/wallet", validateRolMiddleware("cliente"), async (req, res) => {
  const wallet = await Wallets.findOne({cvu: req.user.cvu});
  return res.status(200).json(wallet);
});

app.post("/transference", validateRolMiddleware("cliente"), async (req, res) => {
  const walletOrigin = await Wallets.findOne({cvu: req.user.cvu});

  if (walletOrigin.balance < req.body.monto) {
    return res.status(400).json({
      message: "Balance insuficiente"
    });
  }

  const walletDestination = await Wallets.findOne({cvu: req.body.cvu});

  if (!walletDestination) {
    return res.status(400).json({
      message: "Billetera de destino no existe"
    });
  }

  walletOrigin.balance -= req.body.monto;
  walletDestination.balance += req.body.monto;

  await walletDestination.save();
  await walletOrigin.save();
  
  return res.status(200).send();
});

mongoose.connect("mongodb://mongo:27017/vecindadbank", { useNewUrlParser: true });
loadUsers();

process.on('unhandledRejection', error => {
  console.log(error);
  process.exit(1);
});

app.listen(8000, '0.0.0.0');