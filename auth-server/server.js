"use strict";
const express = require("express");
const app = express();
const fs = require("fs");
const privateKey = fs.readFileSync("./private.pem");
const users = {
  chavodel8: "5d755cda43da9095a2300964",
  señorBarriga: "5d755cf243da9095a2300965",
  admin: "5d755d0043da9095a2300966"
};
const jwt = require("jsonwebtoken");

app.use(express.json());

app.post("/login", (req, res) => {
  if (!Object.keys(users).includes(req.body.username)) {
    return res.status(401).send();
  }

  res.status(200).json({
    jwt: jwt.sign({id: users[req.body.username]}, privateKey, {algorithm: "RS256"})
  });
});

app.listen(8080);