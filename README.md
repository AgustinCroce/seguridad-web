# 1. Instalar VirtualBox (también funciona con VMWare)
https://www.virtualbox.org/wiki/Downloads

# 2. Instalar Vagrant
https://www.vagrantup.com/downloads.html

# 3. Instalar VM
Primero verificar que esté funcionando correctamente:
```
➜ $ vagrant version
Installed Version: 2.2.6
Latest Version: 2.2.6
```

Descargar Vagrantfile con la configuración para la VM:

```
➜ $ curl https://gitlab.com/AgustinCroce/seguridad-web/raw/5eed8a699ae6fecf6c9e318bb88d36d33095ee01/Vagrantfile -o Vagrantfile
```

En la carpeta donde esta ubicado el archivo recién descargado comenzar a instalar la nueva VM:

```
➜ $ vagrant up
```

Esto va a descargar la VM completa con Ubuntu e instalará el proyecto completo con sus dependencias.

Una vez finalizado ejecutar:
```
➜ $ vagrant ssh <<-SHELL
cd seguridad-web-master
sh install.sh
SHELL
```

# 4. Verificar instalación
Deberíamos ver en `localhost:5000` la página con la aplicación web.